import time
import RPi.GPIO as GPIO

SWITCH_PIN = [13]
LED_PIN = [11,7,5,3]
GPIO.setmode(GPIO.BOARD)

GPIO.setup(SWITCH_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)

current_number = 0

try:
    while True:
        
        if GPIO.input(SWITCH_PIN[0]) == GPIO.HIGH:
            current_number = current_number + 1
            
        if current_number == 16:
            current_number = 0
            
        number = current_number
            
        if (number-8) >= 0:
            GPIO.output(LED_PIN[3], GPIO.HIGH)
            number = number - 8
        else:
            GPIO.output(LED_PIN[3], GPIO.LOW)
            
        if (number-4) >= 0:
            GPIO.output(LED_PIN[2], GPIO.HIGH)
            number = number - 4
        else:
            GPIO.output(LED_PIN[2], GPIO.LOW)
            
        if (number-2) >= 0:
            GPIO.output(LED_PIN[1], GPIO.HIGH)
            number = number - 2
        else:
            GPIO.output(LED_PIN[1], GPIO.LOW)
            
        if (number-1) >= 0:
            GPIO.output(LED_PIN[0], GPIO.HIGH)
            number = number - 1
        else:
            GPIO.output(LED_PIN[0], GPIO.LOW)
        
        while True:
            if GPIO.input(SWITCH_PIN[0]) == GPIO.HIGH:
                time.sleep(0.05)
            else:
                break
        
        time.sleep(0.05)
except KeyboardInterrupt:
    print("kb")
finally:
    GPIO.cleanup()
